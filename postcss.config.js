const browsersList = require('./package.json').browserList;

const isDev = process.env.NODE_ENV === 'development' || !process.env.NODE_ENV;

module.exports = {
  sourceMap: true,
  plugins: {
    'precss': {},
    'postcss-cssnext': { browsersList, warnForDuplicates: false },
    'doiuse': {},
    'cssnano': isDev ? false : {
      'preset': [
        'default',
        { 'discardComments': { 'removeAll': true }}
      ]
    }
  }
};