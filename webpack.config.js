const { join, resolve } = require('path');
const { createReadStream } = require('fs');

const webpack = require('webpack');

const HtmlPlugin = require('html-webpack-plugin');
const HtmlTemplatePlugin = require('html-webpack-template');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const isDev = process.env.NODE_ENV === 'development' || !process.env.NODE_ENV;

// PLUGINS

let plugins = [
  new HtmlPlugin({
    filename: 'index.html',
    title: 'Example 1',
    template: HtmlTemplatePlugin,
    inject: false,
    mobile: true,
    appMountId: 'app',
  }),

  new CleanWebpackPlugin('public', { root: __dirname }),

  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: `"${process.env.NODE_ENV}"`,
    },
  }),

  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    filename: '[name].min.js'
  }),

  new ExtractTextPlugin(isDev ? '[name].[hash].css' : '[name].min.css'),
];

if (!isDev) {
  plugins = [
    ...plugins,

    new webpack.SourceMapDevToolPlugin({
      filename: '[file].map',
      exclude: ['vendor']
    }),

    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      drop_debugger: true,
      beautify: false,
      comments: false,
      sourceMap: true
    })
  ]
}

// RULES

let rules = [
  {
    test: /\.js|vue$/,
    enforce: 'pre',
    exclude: /node_modules/,
    loader: 'eslint-loader',
    options: {
      fix: true
    }
  },

  {
    test: /\.vue$/,
    use: [
      {
        loader: 'vue-loader',
        options: {
          loaders: {
            css: ExtractTextPlugin.extract([
              { loader: 'css-loader' },
              { loader: 'postcss-loader', options: { sourceMap: true }}
            ]),
            js: 'babel-loader'
          }
        }
      }
    ]
  },

  {
    test: /\.js$/,
    exclude: /node_modules/,
    use: 'babel-loader',
  },

  {
    test: /\.(png|gif|svg|jp(e)?g)(\?\S*)?$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          name: './static/images/[hash].[ext]',
          limit: 512
        }
      }
    ]
  },

  {
    test: /\.(otf|ttf|eot|woff|woff2)(\?\S*)?$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          name: './static/fonts/[hash].[ext]',
          limit: 50000
        }
      }
    ]
  }
];

// DEV SERVER

let devServer = {
  contentBase: './public/',
  publicPath: '/',
  hot: true,
  port: 9000,
  historyApiFallback: true,
  compress: true,
  clientLogLevel: 'info',
  disableHostCheck: true,
  before (app) {
    app.get('/api/tiles', function(req, res)  {
      res.writeHead(200, { 'Content-Type' : 'application/json' });
      createReadStream(join(process.cwd(), 'api/tiles.json'), { encoding: 'utf-8' })
        .pipe(res);
    });
  }
};

// CONFIG

module.exports = {
  context: __dirname,

  entry: {
    app: join(__dirname, 'src', 'index.js'),
    vendor: ['vue', 'vue-router', 'vuex', 'object-fit-images']
  },

  output: {
    path: join(__dirname, 'public'),
    filename: isDev ? '[name].[id].[hash].js' : '[name].min.js',
    chunkFilename: isDev ? '[id].[hash].js' : '[id].min.js',
    publicPath: '/'
  },

  resolve: {
    extensions: ['*', '.js', '.vue', '.html', '.css'],
    modules: [
      resolve('./src/'),
      resolve('./node_modules'),
    ],
    alias: {
      root: resolve('./src'),
      pages: resolve('./src', 'pages')
    }
  },

  module: { rules },

  plugins,

  devServer
};
