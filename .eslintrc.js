module.exports = {
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:vue/recommended"
  ],
  "plugins": ["vue"],
  "parserOptions": {
    "ecmaVersion": 8,
    "sourceType": "module"
  },
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-debugger": [
      "warn"
    ],
    "no-console": [
      "off"
    ],
    "vue/no-parsing-error": [2, {
      "x-invalid-end-tag": false
    }]
  }
};