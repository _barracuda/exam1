import Vue from 'vue';
import Vuex from 'vuex';
import request from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    articleList: [],
    errors: {
      404: 'Oops! Page not found',
      500: 'Oops! Internal server error'
    }
  },
  mutations: {
    GET_ARTICLE_LIST (state, payload) {
      state.articleList = payload;
    },
    GET_ARTICLE_ITEM (state, payload) {
      state.articleList = [
        ...state.articleList,
        payload
      ];
    }
  },
  actions: {
    async getArticleList ({ commit }) {
      let res = await request.get('/api/tiles');
      commit('GET_ARTICLE_LIST', res.data.tiles);
    },
    async getArticleItem ({ commit }, id) {
      let res = await request.get('/api/tiles');
      let article = res.data.tiles.find(item => item.id === id);
      commit('GET_ARTICLE_ITEM', article);
    }
  }
});