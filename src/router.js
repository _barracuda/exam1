import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from 'pages/Home';
import About from 'pages/About';
import Contacts from 'pages/Contacts';
import Article from 'pages/Article';
import PageError from 'pages/Error';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/article/:id',
      component: Article
    },
    {
      path: '/about',
      component: About
    },
    {
      path: '/contacts',
      component: Contacts
    },
    {
      path: '/error/:code',
      component: PageError
    },
    {
      path: '*',
      redirect: '/error/404'
    }
  ]
});

export default router;