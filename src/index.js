import Vue from 'vue';
import App from 'App.vue';
import router from 'router';
import store from 'store';
import 'object-fit-images';

const app = new Vue({
  store,
  router,
  ...App
});

app.$mount(document.querySelector('#app'));
